/* YPFB.cpp : Este archivo hace referencia a os eventos que va a ejecutar el robot en la pantalla de end.
	Developed by Ing Josmar Jim�nez
	josmar66jimenez@gmail.com
*/

#include <string>
#include <windows.h>
#include <iostream>

using namespace std;

void sleep(string);
int main();
void insert_text(string);

void end(void) {

	string again;

	/*salir de pantalla completa*/
	keybd_event(VK_F11, 0x7A, 0, 0);
	keybd_event(VK_F11, 0x7A, KEYEVENTF_KEYUP, 0); // Release
	sleep("short");
	keybd_event(VK_MENU, 0x12, 0, 0); // Press
	keybd_event(VK_TAB, 0x9, 0, 0); // Press
	keybd_event(VK_TAB, 0x9, KEYEVENTF_KEYUP, 0); // Release
	keybd_event(VK_MENU, 0x12, KEYEVENTF_KEYUP, 0); // Release
	sleep("short");

	cout << "Robot ejecutado sastifactoriamente" << endl;
	cout << " " << endl;
	cout << "Demo desarrollado por Josmar Jimenez" << endl;
	cout << "Email: josmar66jimenez@gmail.com" << endl;
	cout << " " << endl;
	cout << "�Quieres ejecutarlo de nuevo?" << endl;
	cout << "(S) --- (N)" << endl;
	cin >> again;

	sleep("medium");


	if (again == "S" || again == "s") 
		main();
}