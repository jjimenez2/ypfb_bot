/* YPFB.cpp : Este archivo hace referencia a os eventos que va a ejecutar el robot en la pantalla de login.
	Developed by Ing Josmar Jim�nez
	josmar66jimenez@gmail.com
*/

#include <string>
#include <windows.h>
#include <iostream>

using namespace std;

void home(void);
void sleep(string );
void do_click(int, int);
void insert_text(string);

void login(void) {

	sleep("long");

	int userPosX = 830;
	int userPosY = 235;
	int passwordPosX = 830;
	int passwordPosY = 255;
	int buttonEnterPosX = 785;
	int buttonEnterPosY = 295;

	string	username = "SOY UN ROBOT";
	string	password = "PASSWORD";

	// Ejecutando click en usuario
	do_click(userPosX, userPosY);
	sleep("short");
	
	insert_text(username);
	sleep("short");

	do_click(passwordPosX, passwordPosY);

	// Ejecutando click en password
	insert_text(password);
	
	cout << "1--> Haciendo login" << endl;
	sleep("long");

	do_click(buttonEnterPosX, buttonEnterPosY);

	home();
}