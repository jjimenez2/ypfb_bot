/* YPFB.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
	Developed by Ing Josmar Jiménez
	josmar66jimenez@gmail.com
*/

#include <windows.h>
#include <iostream>
#include <string>

using namespace std;

/*Definición de métodos*/
void login(void);
void sleep(string);
/*Definición de variables*/
string url	= "https://ypfb-bot-demo-jjjm.web.app/";

/*Main*/
int main()
{

    std::cout << "Inicializando robot YPFB versi"<<char(162)<<"n demo 0.01\n";
	cout << "Inicializa en 5..." << endl;
	sleep("short");
	cout << "Inicializa en 4..." << endl;
	sleep("short");
	cout << "Inicializa en 3..." << endl;
	sleep("short");
	cout << "Inicializa en 2..." << endl;
	sleep("short");
	cout << "Inicializa en 1..." << endl;
	sleep("short");
	std::cout << "Abriendo navegador por defecto\n";
	ShellExecuteA(GetDesktopWindow(), "open", url.c_str(), NULL, NULL, SW_SHOW);
	std::cout << "Ejecutando pantalla completa\n";
	sleep("medium");
	/*Colocar el browser en pantalla completa*/
	keybd_event(VK_F11, 0x7A, 0, 0);
	keybd_event(VK_F11, 0x7A, KEYEVENTF_KEYUP, 0); // Release
	/*Ir a Login*/
	login();
	return 0;
}
